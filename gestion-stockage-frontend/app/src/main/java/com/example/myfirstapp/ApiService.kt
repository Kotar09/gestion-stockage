package com.example.myfirstapp


import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    @GET("beers")
    fun getBeers(): Call<List<Beers>>

    @GET("stocks")
    fun getStocks(): Call<List<Stocks>>

    @GET("transactions")
    fun getTransactions(): Call<List<Transactions>>

    @GET("orders")
    fun getOrders(): Call<List<Orders>>

    @POST("beers")
    fun addBeer(@Body newBeer: Beers): Call<Beers>

    @GET("stocksToReplenish")
    fun getStocksToReplenish(): Call<List<Stocks>>


    @POST("orders")
    fun createOrder(@Body orderRequest: OrderRequest): Call<Orders>

}
