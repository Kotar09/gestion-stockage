package com.example.myfirstapp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransactionActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)

        showBack()
        setHeaderTitle(getString(R.string.txtTitleTran))

        recyclerView = findViewById(R.id.rvTransactions)
        recyclerView.layoutManager = LinearLayoutManager(this)

        fetchTransactions()
    }

    private fun fetchTransactions() {
        val apiService = ApiClient.apiService
        val call = apiService.getTransactions()

        call.enqueue(object : Callback<List<Transactions>> {
            override fun onResponse(call: Call<List<Transactions>>, response: Response<List<Transactions>>) {
                if (response.isSuccessful) {
                    val transactionsList = response.body() ?: emptyList()
                    adapter = TransactionAdapter(transactionsList)
                    recyclerView.adapter = adapter
                } else {
                    showError("Server returned an error: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<Transactions>>, t: Throwable) {
                t.printStackTrace()
                showError("Failed to load transactions")
            }
        })
    }

    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
