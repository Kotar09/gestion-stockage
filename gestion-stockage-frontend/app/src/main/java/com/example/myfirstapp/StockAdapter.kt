package com.example.myfirstapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class StockAdapter(private val stocks: List<Stocks>) : RecyclerView.Adapter<StockAdapter.StockViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_stock, parent, false)
        return StockViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
        val stock = stocks[position]
        holder.stockBeeId.text = "Beer ID: ${stock.bee_id}"
        holder.stockQuantity.text = "Quantity: ${stock.sto_quantite}"
        holder.stockThreshold.text = "Threshold: ${stock.sto_seuil}"
    }

    override fun getItemCount(): Int {
        return stocks.size
    }

    class StockViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val stockBeeId: TextView = itemView.findViewById(R.id.tvStockBeeId)
        val stockQuantity: TextView = itemView.findViewById(R.id.tvStockQuantity)
        val stockThreshold: TextView = itemView.findViewById(R.id.tvStockThreshold)
    }
}
