package com.example.myfirstapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: OrderAdapter
    private lateinit var fabReplenish: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        showBack()
        setHeaderTitle(getString(R.string.txtTitleCom))

        recyclerView = findViewById(R.id.rvOrders)
        recyclerView.layoutManager = LinearLayoutManager(this)

        fabReplenish = findViewById(R.id.fabReplenish)
        fabReplenish.setOnClickListener {
            val intent = Intent(this, AddReplenishmentActivity::class.java)
            startActivity(intent)
        }

        fetchOrders()
    }

    private fun fetchOrders() {
        val apiService = ApiClient.apiService
        val call = apiService.getOrders()

        call.enqueue(object : Callback<List<Orders>> {
            override fun onResponse(call: Call<List<Orders>>, response: Response<List<Orders>>) {
                if (response.isSuccessful) {
                    val ordersList = response.body() ?: emptyList()
                    adapter = OrderAdapter(ordersList)
                    recyclerView.adapter = adapter
                } else {
                    showError("Server returned an error: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<Orders>>, t: Throwable) {
                t.printStackTrace()
                showError("Failed to load orders")
            }
        })
    }

    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
