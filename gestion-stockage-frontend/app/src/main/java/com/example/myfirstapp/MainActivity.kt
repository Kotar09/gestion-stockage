package com.example.myfirstapp


import android.content.Intent
import android.os.Bundle
import android.widget.Button


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonBeer = findViewById<Button>(R.id.btnViewBeers)
        val buttonStock = findViewById<Button>(R.id.btnManageStock)
        val buttonTransactions = findViewById<Button>(R.id.btnViewTransactions)
        val buttonOrders = findViewById<Button>(R.id.btnManageOrders)

        buttonBeer.setOnClickListener {
            startActivity(Intent(this, BeerListActivity::class.java))
        }

        buttonStock.setOnClickListener {
            startActivity(Intent(this, StockActivity::class.java))
        }

        buttonTransactions.setOnClickListener {
            startActivity(Intent(this, TransactionActivity::class.java))
        }

        buttonOrders.setOnClickListener {
            startActivity(Intent(this, OrderActivity::class.java))
        }
    }
}

