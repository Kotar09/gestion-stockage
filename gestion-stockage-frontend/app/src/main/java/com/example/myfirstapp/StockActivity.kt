package com.example.myfirstapp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StockActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: StockAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock)

        showBack()
        setHeaderTitle(getString(R.string.txtTitleStock))

        recyclerView = findViewById(R.id.rvStocks)
        recyclerView.layoutManager = LinearLayoutManager(this)

        fetchStocks()
    }

    private fun fetchStocks() {
        val apiService = ApiClient.apiService
        val call = apiService.getStocks()

        call.enqueue(object : Callback<List<Stocks>> {
            override fun onResponse(call: Call<List<Stocks>>, response: Response<List<Stocks>>) {
                if (response.isSuccessful) {
                    val stocksList = response.body() ?: emptyList()
                    adapter = StockAdapter(stocksList)
                    recyclerView.adapter = adapter
                } else {
                    showError("Server returned an error: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<Stocks>>, t: Throwable) {
                t.printStackTrace()
                showError("Failed to load stocks")
            }
        })
    }

    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
