package com.example.myfirstapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddBeerActivity : BaseActivity() {

    private lateinit var etBeerName: EditText
    private lateinit var etBeerDescription: EditText
    private lateinit var etBeerVolume: EditText
    private lateinit var btnAddBeer: Button

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_beer)

        showBack()
        setHeaderTitle(getString(R.string.txtTitleAddBe))

        etBeerName = findViewById(R.id.etBeerName)
        etBeerDescription = findViewById(R.id.etBeerDescription)
        etBeerVolume = findViewById(R.id.etBeerVolume)
        btnAddBeer = findViewById(R.id.btnAddBeer)

        btnAddBeer.setOnClickListener {
            val beerName = etBeerName.text.toString().trim()
            val beerDescription = etBeerDescription.text.toString().trim()
            val beerVolume = etBeerVolume.text.toString().trim()

            if (beerName.isNotEmpty() && beerDescription.isNotEmpty() && beerVolume.isNotEmpty()) {
                val newBeer = Beers(0, beerName, beerDescription, beerVolume)
                addBeer(newBeer)
            } else {
                Toast.makeText(this, "Please fill out all fields", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun addBeer(newBeer: Beers) {
        val apiService = ApiClient.apiService
        val call = apiService.addBeer(newBeer)

        call.enqueue(object : Callback<Beers> {
            override fun onResponse(call: Call<Beers>, response: Response<Beers>) {
                if (response.isSuccessful) {
                    Toast.makeText(this@AddBeerActivity, "Beer added successfully", Toast.LENGTH_SHORT).show()
                    setResult(RESULT_OK)
                    finish() // Close the activity
                } else {
                    showError("Failed to add beer: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<Beers>, t: Throwable) {
                t.printStackTrace()
                showError("Failed to add beer")
            }
        })
    }

    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
