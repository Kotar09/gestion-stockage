package com.example.myfirstapp

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


open class BaseActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("Project","########## onCreate ##########"+this.javaClass.simpleName)
    }

    override fun onStart() {
        super.onStart()
        Log.d("Project","########## onStart ##########"+this.javaClass.simpleName)
    }

    override fun onResume() {
        super.onResume()
        Log.d("Project","########## onResume ##########"+this.javaClass.simpleName)
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("Project","########## onRestart ##########"+this.javaClass.simpleName)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("Project","########## onDestroy ##########"+this.javaClass.simpleName)
    }

    override fun onStop() {
        super.onStop()
        Log.d("Project","########## onStop ##########"+this.javaClass.simpleName)
    }

    override fun onPause() {
        super.onPause()
        Log.d("Project","########## onPause ##########"+this.javaClass.simpleName)
    }
    override fun finish() {
        super.finish()
        Log.d("Project","########## finish ##########"+this.javaClass.simpleName)
    }
    fun showBack() {
        val imageViewBack = findViewById<ImageView>(R.id.imageViewBack)
        imageViewBack.visibility = View.VISIBLE
        imageViewBack.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    fun setHeaderTitle(title:String?, imageResId: Int? = null){
        val textView = findViewById<TextView>(R.id.textViewTitle)


        if (title != null && imageResId == null) {
            textView.visibility = View.VISIBLE

            textView.text = title
        } else if (imageResId != null) {
            textView.visibility = View.GONE

        }
    }

}