package com.example.myfirstapp

class Beers
    (val bee_id: Int,
    val bee_nom: String,
    val bee_description: String,
    val bee_volume: String
)