package com.example.myfirstapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerListActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: BeersAdapter
    private lateinit var addBeerLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer_list)

        showBack()
        setHeaderTitle(getString(R.string.txtTitleBiere))

        recyclerView = findViewById(R.id.rvBeers)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = BeersAdapter { beer ->
            // Handle item click
            Toast.makeText(this, "Clicked on: ${beer.bee_nom}", Toast.LENGTH_SHORT).show()
        }
        recyclerView.adapter = adapter

        val fabAddBeer: FloatingActionButton = findViewById(R.id.fabAddBeer)
        fabAddBeer.setOnClickListener {
            val intent = Intent(this, AddBeerActivity::class.java)
            addBeerLauncher.launch(intent)
        }

        addBeerLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if (result.resultCode == RESULT_OK) {
                // Refresh the list of beers when returning from AddBeerActivity
                fetchBeers()
            }
        }

        fetchBeers()
    }

    private fun fetchBeers() {
        val apiService = ApiClient.apiService
        val call = apiService.getBeers()

        call.enqueue(object : Callback<List<Beers>> {
            override fun onResponse(call: Call<List<Beers>>, response: Response<List<Beers>>) {
                if (response.isSuccessful) {
                    val beersList = response.body() ?: emptyList()
                    adapter.setBeers(beersList)
                } else {
                    showError("Server returned an error: ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<Beers>>, t: Throwable) {
                t.printStackTrace()
                showError("Failed to load beers")
            }
        })
    }


    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
