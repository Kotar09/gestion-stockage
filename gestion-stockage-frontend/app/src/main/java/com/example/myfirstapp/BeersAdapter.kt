package com.example.myfirstapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myfirstapp.databinding.ItemBeerBinding

class BeersAdapter(private val clickListener: (Beers) -> Unit) : RecyclerView.Adapter<BeersAdapter.BeerViewHolder>() {

    private var beers: List<Beers> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        val binding = ItemBeerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BeerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        holder.bind(beers[position], clickListener)
    }

    override fun getItemCount(): Int = beers.size

    fun setBeers(beers: List<Beers>) {
        this.beers = beers
        notifyDataSetChanged()
    }

    class BeerViewHolder(private val binding: ItemBeerBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(beer: Beers, clickListener: (Beers) -> Unit) {
            binding.tvBeerName.text = beer.bee_nom
            binding.tvBeerDescription.text = beer.bee_description
            binding.tvBeerVolume.text = beer.bee_volume
            binding.root.setOnClickListener { clickListener(beer) }
        }
    }
}
