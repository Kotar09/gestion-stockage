package com.example.myfirstapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class ReplenishmentAdapter(private val items: MutableList<ReplenishmentItem>) :
    RecyclerView.Adapter<ReplenishmentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_replenishment, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = items.size

    fun updateItems(newItems: List<ReplenishmentItem>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvBeerId: TextView = itemView.findViewById(R.id.tvBeerId)
        private val etQuantity: EditText = itemView.findViewById(R.id.etQuantity)
        private val btnRemove: Button = itemView.findViewById(R.id.btnRemove)

        fun bind(item: ReplenishmentItem) {
            tvBeerId.text = item.bee_id.toString()
            etQuantity.setText(item.lig_com_quantite.toString())

            btnRemove.setOnClickListener {
                items.removeAt(adapterPosition)
                notifyItemRemoved(adapterPosition)
            }
        }
    }
}
