package com.example.myfirstapp


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class OrderAdapter(private val orders: List<Orders>) : RecyclerView.Adapter<OrderAdapter.OrderViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_order, parent, false)
        return OrderViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        val order = orders[position]
        holder.orderDate.text = "Date: ${order.com_date}"
        holder.orderStatus.text = "Status: ${order.com_statut}"
        holder.orderUserId.text = "User ID: ${order.uti_id}"
    }

    override fun getItemCount(): Int {
        return orders.size
    }

    class OrderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val orderDate: TextView = itemView.findViewById(R.id.tvOrderDate)
        val orderStatus: TextView = itemView.findViewById(R.id.tvOrderStatus)
        val orderUserId: TextView = itemView.findViewById(R.id.tvOrderUserId)
    }
}
