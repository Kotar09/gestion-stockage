package com.example.myfirstapp

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddReplenishmentActivity : BaseActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: ReplenishmentAdapter
    private lateinit var replenishmentItems: MutableList<ReplenishmentItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_replenishment)

        showBack()
        setHeaderTitle(getString(R.string.txtTitleAddR))

        recyclerView = findViewById(R.id.rvReplenishmentItems)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = ReplenishmentAdapter(mutableListOf()) // Initialize adapter with empty list
        recyclerView.adapter = adapter // Set adapter to RecyclerView

        fetchStocksToReplenish()

        findViewById<Button>(R.id.btnValidateOrder).setOnClickListener {
            validateOrder()
        }
    }

    private fun fetchStocksToReplenish() {
        val apiService = ApiClient.apiService
        val call = apiService.getStocksToReplenish()

        call.enqueue(object : Callback<List<Stocks>> {
            override fun onResponse(call: Call<List<Stocks>>, response: Response<List<Stocks>>) {
                if (response.isSuccessful) {
                    val stocks = response.body()
                    stocks?.let {
                        replenishmentItems = it.map { stock ->
                            ReplenishmentItem(stock.bee_id, calculateReplenishmentQuantity(stock))
                        }.toMutableList()
                        adapter.updateItems(replenishmentItems) // Update adapter with fetched items
                    }
                } else {
                    showError("Failed to fetch stocks to replenish: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<List<Stocks>>, t: Throwable) {
                t.printStackTrace()
                showError("Failed to fetch stocks to replenish")
            }
        })
    }

    private fun calculateReplenishmentQuantity(stock: Stocks): Int {
        return stock.sto_seuil + 1 - stock.sto_quantite
    }

    private fun validateOrder() {
        val apiService = ApiClient.apiService
        val orderRequest = OrderRequest(replenishmentItems)
        val call = apiService.createOrder(orderRequest)

        call.enqueue(object : Callback<Orders> {
            override fun onResponse(call: Call<Orders>, response: Response<Orders>) {
                if (response.isSuccessful) {
                    Toast.makeText(this@AddReplenishmentActivity, "Order validated successfully", Toast.LENGTH_SHORT).show()
                    finish()
                } else {
                    val errorMessage = "Failed to validate order: ${response.message()}"
                    showError(errorMessage)
                }
            }

            override fun onFailure(call: Call<Orders>, t: Throwable) {
                t.printStackTrace()
                showError("Failed to validate order: ${t.message}")
            }
        })
    }



    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
