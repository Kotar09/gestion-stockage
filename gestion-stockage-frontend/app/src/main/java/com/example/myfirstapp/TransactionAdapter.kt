package com.example.myfirstapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class TransactionAdapter(private val transactions: List<Transactions>) : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_transaction, parent, false)
        return TransactionViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val transaction = transactions[position]
        holder.transactionDate.text = "Date: ${transaction.tran_date}"
        holder.transactionQuantity.text = "Quantity: ${transaction.tran_quantite}"
        holder.transactionBeeId.text = "Beer ID: ${transaction.bee_id}"
    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val transactionDate: TextView = itemView.findViewById(R.id.tvTransactionDate)
        val transactionQuantity: TextView = itemView.findViewById(R.id.tvTransactionQuantity)
        val transactionBeeId: TextView = itemView.findViewById(R.id.tvTransactionBeeId)
    }
}
