const express = require('express');
const config = require('./config/config');
const sequelize = require('./config/database');

const app = express();

app.use(express.json());


const beerRoutes = require('./routes/beerRoutes');
const orderRoutes = require('./routes/orderRoutes');
const stockRoutes = require('./routes/stockRoutes');
const transactionRoutes = require('./routes/transactionsRoutes');
const userRoutes = require('./routes/userRoutes');

app.use('/api', beerRoutes);
app.use('/api', orderRoutes);
app.use('/api', stockRoutes);
app.use('/api', transactionRoutes);
app.use('/api', userRoutes);

const PORT = config.app.port;

sequelize.sync().then(() => {
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
}).catch((error) => {
  console.log('Unable to connect to the database:', error);
});



