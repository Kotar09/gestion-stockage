const Stock = require('../models/stockModel');
const Sequelize = require('sequelize');
const { Op } = Sequelize;

const getAllStocks = async () => {
    return await Stock.findAll();
};

const getStockById = async (id) => {
    return await Stock.findByPk(id);
};

const getStocksToReplenish = async () => {
    return await Stock.findAll({
        where: {
            sto_quantite: {
                [Op.lt]: Sequelize.col('sto_seuil')
            }
        }
    });
};

const createStock = async (stockData) => {
    return await Stock.create(stockData);
};

const updateStock = async (id, stockData) => {
    const stock = await Stock.findByPk(id);
    if (stock) {
        return await stock.update(stockData);
    }
    return null;
};

const deleteStock = async (id) => {
    const stock = await Stock.findByPk(id);
    if (stock) {
        await stock.destroy();
        return true;
    }
    return false;
};

module.exports = {
    getAllStocks,
    getStockById,
    createStock,
    updateStock,
    deleteStock,
    getStocksToReplenish,
};
