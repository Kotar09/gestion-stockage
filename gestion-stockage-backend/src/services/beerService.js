const Beer = require('../models/beerModel');

exports.getAllBeers = async () => {
  return await Beer.findAll();
};

exports.getBeerById = async (id) => {
  return await Beer.findByPk(id);
};

exports.createBeer = async (beer) => {
  return await Beer.create(beer);
};

exports.updateBeer = async (id, beer) => {
  const existingBeer = await Beer.findByPk(id);
  if (existingBeer) {
    return await existingBeer.update(beer);
  }
  throw new Error('Beer not found');
};

exports.deleteBeer = async (id) => {
  const existingBeer = await Beer.findByPk(id);
  if (existingBeer) {
    await existingBeer.destroy();
    return true;
  }
  throw new Error('Beer not found');
};