const Transaction = require('../models/transactionsModel');

const getAllTransactions = async () => {
    return await Transaction.findAll();
};

const getTransactionById = async (id) => {
    return await Transaction.findByPk(id);
};

const createTransaction = async (transactionData) => {
    return await Transaction.create(transactionData);
};

const updateTransaction = async (id, transactionData) => {
    const transaction = await Transaction.findByPk(id);
    if (transaction) {
        return await transaction.update(transactionData);
    }
    return null;
};

const deleteTransaction = async (id) => {
    const transaction = await Transaction.findByPk(id);
    if (transaction) {
        await transaction.destroy();
        return true;
    }
    return false;
};

module.exports = {
    getAllTransactions,
    getTransactionById,
    createTransaction,
    updateTransaction,
    deleteTransaction
};
