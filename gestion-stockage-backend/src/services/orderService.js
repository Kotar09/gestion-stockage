const Order = require('../models/orderModel');

const getAllOrders = async () => {
    return await Order.findAll();
};

const getOrderById = async (id) => {
    return await Order.findByPk(id);
};

const createOrder = async (orderData) => {
    return await Order.create(orderData);
};

const updateOrder = async (id, orderData) => {
    const order = await Order.findByPk(id);
    if (order) {
        return await order.update(orderData);
    }
    return null;
};

const deleteOrder = async (id) => {
    const order = await Order.findByPk(id);
    if (order) {
        await order.destroy();
        return true;
    }
    return false;
};

module.exports = {
    getAllOrders,
    getOrderById,
    createOrder,
    updateOrder,
    deleteOrder
};
