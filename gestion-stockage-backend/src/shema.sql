-- table type_beer
CREATE TABLE IF NOT EXISTS type_beer (
    bee_id INT AUTO_INCREMENT PRIMARY KEY,
    bee_nom VARCHAR(50) NOT NULL,
    bee_description VARCHAR(50),
    bee_volume VARCHAR(50)
);

-- table stock
CREATE TABLE IF NOT EXISTS stock (
    sto_id INT AUTO_INCREMENT PRIMARY KEY,
    sto_quantite INT,
    sto_seuil INT,
    bee_id INT NOT NULL,
    FOREIGN KEY (bee_id) REFERENCES type_beer(bee_id)
);

-- table transactions
CREATE TABLE IF NOT EXISTS transactions (
    tran_id INT AUTO_INCREMENT PRIMARY KEY,
    tran_date DATE,
    tran_quantite INT,
    bee_id INT NOT NULL,
    FOREIGN KEY (bee_id) REFERENCES type_beer(bee_id)
);

-- table utilisateurs
CREATE TABLE IF NOT EXISTS utilisateurs (
    uti_id INT AUTO_INCREMENT PRIMARY KEY,
    uti_nom VARCHAR(50),
    uti_mot_de_passe VARCHAR(50),
    uti_role VARCHAR(50)
);

-- table commandes
CREATE TABLE IF NOT EXISTS commandes (
    com_id INT AUTO_INCREMENT PRIMARY KEY,
    com_date DATE,
    com_statut VARCHAR(50),
    uti_id INT NOT NULL,
    FOREIGN KEY (uti_id) REFERENCES utilisateurs(uti_id)
);

-- table ligne_commande
CREATE TABLE IF NOT EXISTS ligne_commande (
    lig_com_id INT AUTO_INCREMENT PRIMARY KEY,
    lig_com_quantite INT,
    com_id INT NOT NULL,
    bee_id INT NOT NULL,
    FOREIGN KEY (com_id) REFERENCES commandes(com_id),
    FOREIGN KEY (bee_id) REFERENCES type_beer(bee_id)
);
