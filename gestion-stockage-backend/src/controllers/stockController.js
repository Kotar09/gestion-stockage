const stockService = require('../services/stockService');

const getAllStocks = async (req, res) => {
    try {
        const stocks = await stockService.getAllStocks();
        res.status(200).json(stocks);
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving stocks', error });
    }
};

const getStockById = async (req, res) => {
    try {
        const stock = await stockService.getStockById(req.params.id);
        if (stock) {
            res.status(200).json(stock);
        } else {
            res.status(404).json({ message: 'Stock not found' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving stock', error });
    }
};

const getStocksToReplenish = async (req, res) => {
    try {
        const stocks = await stockService.getStocksToReplenish();
        res.status(200).json(stocks);
    } catch (error) {
        console.error('Error fetching stocks to replenish:', error);
        res.status(500).json({ message: 'Error fetching stocks to replenish', error });
    }
};

const createStock = async (req, res) => {
    try {
        const newStock = await stockService.createStock(req.body);
        res.status(201).json(newStock);
    } catch (error) {
        res.status(500).json({ message: 'Error creating stock', error });
    }
};

const updateStock = async (req, res) => {
    try {
        const updatedStock = await stockService.updateStock(req.params.id, req.body);
        if (updatedStock) {
            res.status(200).json(updatedStock);
        } else {
            res.status(404).json({ message: 'Stock not found' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Error updating stock', error });
    }
};

const deleteStock = async (req, res) => {
    try {
        const success = await stockService.deleteStock(req.params.id);
        if (success) {
            res.status(200).json({ message: 'Stock deleted' });
        } else {
            res.status(404).json({ message: 'Stock not found' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Error deleting stock', error });
    }
};

module.exports = {
    getAllStocks,
    getStockById,
    createStock,
    updateStock,
    deleteStock,
    getStocksToReplenish,
};
