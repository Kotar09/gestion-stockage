const beerService = require('../services/beerService');

exports.getAllBeers = async (req, res) => {
  try {
    const beers = await beerService.getAllBeers();
    res.status(200).json(beers);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.getBeerById = async (req, res) => {
  try {
    const beer = await beerService.getBeerById(req.params.id);
    if (beer) {
      res.status(200).json(beer);
    } else {
      res.status(404).json({ message: 'Beer not found' });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.createBeer = async (req, res) => {
  const { bee_nom, bee_description, bee_volume } = req.body;
  try {
    const beer = await beerService.createBeer({
      bee_nom,
      bee_description,
      bee_volume
    });
    res.status(201).json(beer);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.updateBeer = async (req, res) => {
  const { id } = req.params;
  const { bee_nom, bee_description, bee_volume } = req.body;
  try {
    const beer = await beerService.updateBeer(id, {
      bee_nom,
      bee_description,
      bee_volume
    });
    res.status(200).json(beer);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.deleteBeer = async (req, res) => {
  const { id } = req.params;
  try {
    await beerService.deleteBeer(id);
    res.status(204).send();
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
