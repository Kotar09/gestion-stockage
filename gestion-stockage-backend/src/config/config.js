const dotenv = require('dotenv');

dotenv.config();

module.exports = {
  app: {
    port: process.env.PORT || 3000,
  },
  db: {
    name: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT || 'mysql', 
  },
  jwt: {
    secret: process.env.JWT_SECRET || 'your_secret_key',
    expiresIn: process.env.JWT_EXPIRES_IN || '1h',
  },
  api: {
    someExternalApiKey: process.env.SOME_EXTERNAL_API_KEY,
  },
};
