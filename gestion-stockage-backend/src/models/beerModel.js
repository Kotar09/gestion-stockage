const { Sequelize, DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Beer = sequelize.define('Beer', {
  bee_id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  bee_nom: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  bee_description: {
    type: DataTypes.STRING,
  },
  bee_volume: {
    type: DataTypes.DECIMAL(10, 2),
  },
}, {
  tableName: 'type_beer',
  timestamps: false,
});

module.exports = Beer;
