const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Order = sequelize.define('Order', {
    com_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    com_date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    com_statut: {
        type: DataTypes.STRING,
        allowNull: false
    },
    uti_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'utilisateurs',
            key: 'uti_id'
        }
    }
}, {
    tableName: 'commandes',
    timestamps: false
});

module.exports = Order;
