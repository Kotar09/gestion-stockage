const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Beer = require('./beerModel');

const Transaction = sequelize.define('Transaction', {
    tran_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tran_date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    tran_quantite: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    bee_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: Beer,
            key: 'bee_id'
        }
    }
}, {
    tableName: 'transactions',
    timestamps: false
});

Beer.hasMany(Transaction, { foreignKey: 'bee_id' });
Transaction.belongsTo(Beer, { foreignKey: 'bee_id' });

module.exports = Transaction;
