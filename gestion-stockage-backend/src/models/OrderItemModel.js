const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const OrderItem = sequelize.define('OrderItem', {
    lig_com_id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  com_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  bee_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  lig_com_quantite: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
});

module.exports = OrderItem;
