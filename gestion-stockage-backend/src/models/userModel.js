const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const User = sequelize.define('User', {
    uti_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    uti_nom: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    uti_mot_de_passe: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    uti_role: {
        type: DataTypes.STRING(50),
        allowNull: false
    }
}, {
    tableName: 'utilisateurs',
    timestamps: false
});

module.exports = User;
