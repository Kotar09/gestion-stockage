const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');
const Beer = require('./beerModel');

const Stock = sequelize.define('Stock', {
    sto_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    sto_quantite: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    sto_seuil: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    bee_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'type_beer',
            key: 'bee_id'
        }
    }
}, {
    tableName: 'stock',
    timestamps: false
});

Beer.hasMany(Stock, { foreignKey: 'bee_id' });
Stock.belongsTo(Beer, { foreignKey: 'bee_id' });

module.exports = Stock;
