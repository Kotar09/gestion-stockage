const express = require('express');
const router = express.Router();
const beerController = require('../controllers/beerController');


router.get('/beers', beerController.getAllBeers);
router.get('/beers/:id', beerController.getBeerById);
router.post('/beers', beerController.createBeer);
router.put('/beers/:id', beerController.updateBeer);
router.delete('/beers/:id', beerController.deleteBeer);

module.exports = router;




