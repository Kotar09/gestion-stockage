# Schema Relationnelle MLD

![alt text](img/image.png)


# Installation des dépendences Nécessaire 

## Framework Express.js
```sh
    npm install express
```

## Base de donnée
```sh
    npm install mysql2
```

## ORM 
```sh
npm install sequelize
```
## Chargement des variables d'environnement à partir d'un fichier .env.

```sh
npm install dotenv
```

# Configuration de la base de données

- Créez un fichier `.env` dans le dossier `gestion-stockage-backend`.

- Copiez et collez le contenu suivant dans `.env` :

```.env
# Server
PORT=3000

# Database
DB_NAME=Biere
DB_USER= Votre user de votre base de donnée
DB_PASSWORD= Votre Mot De passe de votre base de donnée
DB_HOST=localhost
DB_PORT=3306
DB_DIALECT=mysql

# JWT
JWT_SECRET=myjwtsecret
JWT_EXPIRES_IN=1h

# External API
SOME_EXTERNAL_API_KEY=myexternalapikey
```
# Lancement du serveur

```sh
node run start
```
# Test d'API sur Insomnia

![alt text](img/image-2.png)
