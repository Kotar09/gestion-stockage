# Projet Archi-N-Tiers

## Architecture du projet

![alt text](img/diagramme_architecture_exo_4_coef_2.png)

## Choix de la Technologie Niveau Backend, Express qui est un framework minimaliste basé sur Node.js

### Avantages :

- Léger et rapide pour les applications en temps réel.
- Facile à configurer et à démarrer.
- Grande communauté et beaucoup de modules disponibles via npm.


### Stack :

- Langage : JavaScript
- Framework : Express.js
- ORM : Sequelize 
- Base de données : MySQL
- Outils de construction : npm 

## Choix de la Technologie Niveau Frontend : Android Studio pour Développer une Application Mobile

### Avantages :
- Les employés utilisent des tablettes pour leurs tâches quotidiennes. Il est donc essentiel de développer une application qui soit compatible avec ces dispositifs.

- **Maintenance et Évolutivité :** Avec Android Studio, les mises à jour et les nouvelles fonctionnalités peuvent être ajoutées de manière plus rapide et plus sécurisée. 

### Stack :
- Langage : Kotlin; Kotlin est recommandé pour les nouvelles applications en raison de ses avantages modernes
- IDE : Android Studio
- API : Les appels API et la communication avec le backend